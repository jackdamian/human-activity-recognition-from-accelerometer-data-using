# README #

This repository is for the development of a pipeline for unsupervised learning on accelerometer data from the SMOVE project(https://smove.tech/).

### How do I get set up? ###

#### Installing required packages ####

First install the packages in requirements.txt:

```
pip install -r requirements.txt 
```

Then install pyhsmm. Try installing using:

```
pip install pyhsmm
```

If this does not work, then the reposity has to be downloaded from https://github.com/mattjj/pyhsmm Navigate to the location of the pyhsmm repository and run the following command:

```
python setup.py install
```

This may result in another error, when installing the package pybasicbayes.
In order to install pybasicbayes download the repository from https://github.com/mattjj/pybasicbayes and then run the previous command, but then inside the pybasicbayes repository. After this. The previous step can be followed again. 

Now you have all necessary packages installed.

Start jupyter notebook using:

```
jupyter notebook
```

#### Example runs ####

Look at the example runs located at /Notebooks/Example runs/ to see expected results from running the notebooks.

