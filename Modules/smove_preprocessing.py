# This module contains functions for pre processing accelerometer data of the SMOVE project

# IMPORTS
import numpy as np


# OBJECTS
class SmoveMinMaxNormalizer():
    def __init__(self):
        self.min_ENMO = float("inf")
        self.max_ENMO = -float("inf")
        self.min_angle_X = float("inf")
        self.max_angle_X = -float("inf")
        self.min_angle_Y = float("inf")
        self.max_angle_Y = -float("inf")
        self.min_angle_Z = float("inf")
        self.max_angle_Z = -float("inf")

        
    def update_var(self, variable_name, new_value):
        """
        Takes a value and compares it to the current min and max for that variable, if the value is a new min or max,
        then that variable gets updated to that new value.
        Parameters
        ------
        variable_name : string
            The variable name of the given value, must be either ENMO, angle_X, angle_Y, or angle_Z.
        new_value : float
            A new float value that represents either ENMO, angle_X, angle_Y, or angle_Z.
        """
        
        if variable_name == "ENMO":
            self.min_ENMO = min(self.min_ENMO, np.min(new_value))
            self.max_ENMO = max(self.max_ENMO, np.max(new_value))
        elif variable_name == "angle_X":
            self.min_angle_X = min(self.min_angle_X, np.min(new_value))
            self.max_angle_X = max(self.max_angle_X, np.max(new_value))
        elif variable_name == "angle_Y":
            self.min_angle_Y = min(self.min_angle_Y, np.min(new_value))
            self.max_angle_Y = max(self.max_angle_Y, np.max(new_value))
        elif variable_name == "angle_Z":
            self.min_angle_Z = min(self.min_angle_Z, np.min(new_value))
            self.max_angle_Z = max(self.max_angle_Z, np.max(new_value))
        else:
            raise Exception("Variable name must be: ENMO, angle_X, angle_Y, or angle_Z")
        
        
    def normalize(self, variable_name, value):
        """
        Normalizes a given value based on the min and max of the variable (min-max normalization).
        Parameters
        ------
        variable_name : string
            The variable name of the given value, must be either ENMO, angle_X, angle_Y, or angle_Z.
        value : float
            A float that represents either ENMO, angle_X, angle_Y, or angle_Z.
        Returns
        ------
        normalized_value : float
            The min-max normalized version of the given input value
        """
        
        if variable_name == "ENMO":
            normalized_value = (value - self.min_ENMO) / (self.max_ENMO - self.min_ENMO)
        elif variable_name == "angle_X":
            normalized_value = (value - self.min_angle_X) / (self.max_angle_X - self.min_angle_X)
        elif variable_name == "angle_Y":
            normalized_value = (value - self.min_angle_Y) / (self.max_angle_Y - self.min_angle_Y)
        elif variable_name == "angle_Z":
            normalized_value = (value - self.min_angle_Z) / (self.max_angle_Z - self.min_angle_Z)
        else:
            raise Exception("Variable name must be: ENMO, angle_X, angle_Y, or angle_Z")
        return normalized_value


# FUNCTIONS
def calc_ENMO(x, y, z):
    """
    Calculates the ENMO (Euclidean Norm Minus One with negative values rounded to zero), calculated from 
    three given values: X, Y and Z.
    Parameters
    ------
    x, y and z : float
        A float which describes the acceleration in positive or negative direction based on the axis (X, Y or Z).
    Returns
    ------
    ENMO (Euclidean Norm Minus One with negative values rounded to zero)
    """
    
    euclidean_norm = np.sqrt(x**2 + y**2 + z**2)
    return np.clip(euclidean_norm - 1, 0, None)


def calc_acceleration_angle(main_axis, second_axis, third_axis):
    """
    Calculates the orientation angles of the acceleration axis relative to the horizontal plane.
    Parameters
    ------
    main_axis, second_axis and third_axis : float
        A float which describes the acceleration in positive or negative direction based on the axis (X, Y or Z).
    Returns
    ------
    ENMO (Euclidean Norm Minus One with negative values rounded to zero)
    """
    
    return np.degrees(np.arctan(main_axis / np.sqrt(second_axis**2 + third_axis**2)))


def divide_data_into_windows(sensor_name, sensor_data, nrows_per_window):
    """
    Divides the data from a given device into movement windows.
    Parameters
    ------
    sensor_name : string
        The name of the given device. this could for example be "left_wrist".
    sensor_data : pandas DataFrame
        A dataframe containing the timestamps, labels, accelerometer axes data and created features from a device.
    nrows_per_window : integer
        An integer representing the amount of datapoints a window wil have to contain.
    Returns
    ------
    A pandas DataFrama containing a timestamp, label, and a mean of each feature for every movement window. 
    """
    
    # First we create groups of the dataset using 2 factors, the number of rows per window and the labels
    # This will create many windows of n_rows but also others smaller, because a label changed in the middle of a window, 
    # that we don't want.
    # there were also incomplete windows to begin with because we already eliminated all the lab task (label < 5)
    df_grouped = sensor_data.groupby([sensor_data.index // nrows_per_window,'label'])
    
    # Now we filter these windows and we keep only those with a len equal to nrows_per_window. 
    # This actually returns the original dataset, but without the entries of the groups that we just eliminated
    original_df_without_small_groups = df_grouped.filter(lambda x: len(x) == nrows_per_window)
    
    # We apply again the grouping, but now we don't need to do it by label again,
    # because we have already eliminated these (I have checked, it works)
    df_grouped_filtered = original_df_without_small_groups.groupby([original_df_without_small_groups.index // nrows_per_window])
    
    # we keep only the columns we are interested in
    # then we apply min to the timestamp, mean to the data and min to the label (all the labels of the group are the same)
    # finally we rename the columns to prepare for the next step
    return df_grouped_filtered[['timestamp','label', 'n_ENMO', 'n_angle_X', 'n_angle_Y', 'n_angle_Z']]\
    .agg({'timestamp' : 'min', 'label':'min', 'n_ENMO':'mean','n_angle_X':'mean','n_angle_Y':'mean','n_angle_Z':'mean'})\
    .rename(columns={"n_ENMO": sensor_name + "_mean_ENMO", 
                     "n_angle_X": sensor_name + "_mean_anglex", 
                     "n_angle_Y": sensor_name + "_mean_angley", 
                     "n_angle_Z": sensor_name + "_mean_anglez"})
