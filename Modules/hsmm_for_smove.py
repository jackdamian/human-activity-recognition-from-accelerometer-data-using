# This module contains functions for using an HSMM on data of the SMOVE project using the pyhsmm package.

# IMPORTS
from collections import Counter
import itertools
from matplotlib import pyplot as plt
from matplotlib.collections import LineCollection
import numpy as np
import pandas as pd
import pyhsmm
import pyhsmm.basic.distributions as distributions
from pyhsmm.util.text import progprint_xrange
from scipy import stats
import smove_label_dictionaries as dicts


# OBJECTS
class ModelResultManager():
    def __init__(self, stateseqs, features):
        self.state_seqs = stateseqs
        self.features = features
        self.label_seqs = [participant["label"] for participant in features.values()]
        self.str_label_seqs = self.label_to_string(simplified=False)
        self.str_label_seqs_simplified = self.label_to_string(simplified=True)
        self.str_state_seqs = assign_label_to_states(self.state_seqs, self.str_label_seqs)
        self.str_state_seqs_simplified = assign_label_to_states(self.state_seqs, self.str_label_seqs_simplified)

        
    def label_to_string(self, simplified=False):
        """
        Returns string versions of the label sequences, using the float label sequences and a dictionary.
        Can return either detailed string labels or simplified string labels.
        Parameters
        ------
        simplified : boolean
            boolean, which determines if the string versions of the labels are going to be simplified
        Returns
        ------
        new_labels : list
            A list containing all of the label sequences in string form.
        """
        
        if simplified:
            used_dictionary = dicts.label_simplified_dict
        else:
            used_dictionary = dicts.label_dict
        new_labels = []
        for participant in self.label_seqs:
            new_labels.append([used_dictionary[window] for window in participant])
        return new_labels

    
    def print_mislabel_chart(self, use_simplified_states=True):
        """
        Prints the counts of mislabel combinations between the hidden states and the actual labels.  
        Parameters
        ------
        use_simplified_states : boolean
            boolean, which determines if simplified labels are used.
        """
        
        combinations = []
        incorrect_combinations = []
        count = 0
        correct = 0
        incorrect = 0
        if use_simplified_states:
            string_state_sequence = self.str_state_seqs_simplified
            string_label_sequence = self.str_label_seqs_simplified
        else:
            string_state_sequence = self.str_state_seqs
            string_label_sequence = self.str_label_seqs
        print("mislabel count \t (actual label,  state label)")
        for part_index in range(len(self.label_seqs)):
            for state in string_state_sequence[part_index]:
                combinations.append((string_label_sequence[part_index][count], state))
                if state == string_label_sequence[part_index][count]:
                    correct += 1
                else:
                    incorrect += 1
                    incorrect_combinations.append((self.str_label_seqs[part_index][count], state))
                count += 1
            count = 0
        print("-------------------------------------------------------")
        for combination in Counter(incorrect_combinations).most_common():
            print(combination[1], "\t", combination[0])
        print("\nModel percentage correct:", correct / (correct + incorrect))

        
    def crosstab_labels_str_states_simplified(self):
        """
        Returns a confusion matrix between the hidden states  and the actual labels, using the simplified label variants  
        for the hidden states
        """
        
        y_actual = pd.Series(list(itertools.chain(*self.str_label_seqs_simplified)), name='Actual')
        y_predict = pd.Series(list(itertools.chain(*self.str_state_seqs_simplified)), name='Predicted')
        return pd.crosstab(y_actual, y_predict)

    
    def crosstab_labels_int_states(self, simplified=True):
        """
        Returns a confusion matrix between the hidden states  and the actual labels, 
        using the actual int id's for the hidden states.
        Parameters
        ------
        simplified : boolean
            boolean, which determines if simplified label variants are used for the movement labels.
        """
        
        if simplified:
            y_actual = pd.Series(list(itertools.chain(*self.str_label_seqs_simplified)), name='Actual')
        else:
            y_actual = pd.Series(list(itertools.chain(*self.str_label_seqs)), name='Actual')
        y_predict = pd.Series(list(itertools.chain(*self.state_seqs)), name='Predicted')
        return pd.crosstab(y_actual, y_predict)

    
    def plot_sequence_comparison(self, part_index):
        """
        Plots a coloured figure, which compares the hidden state sequence with the actual movement label sequence for a given
        participant.
        Parameters
        ------
        part_index : integer
            identifying integer which corresponds to a certain participant in the data
        """
        
        n = len(self.label_seqs[part_index])
        x = np.arange(n + 1)  # resampledTime
        y = "1" * n  # modulusOfZeroNormalized

        # set up colors
        c_states = [dicts.state_color_dict[x] for x in self.str_state_seqs_simplified[part_index]]
        c_labels = [dicts.label_color_dict[x] for x in self.label_seqs[part_index]]

        # convert time series to line segments
        lines = [((x0, y0), (x1, y1)) for x0, y0, x1, y1 in zip(x[:-1], y[:-1], x[1:], y[1:])]
        colored_lines_states = LineCollection(lines, colors=c_states, linewidths=(150,))
        colored_lines_labels = LineCollection(lines, colors=c_labels, linewidths=(150,))

        # get participant IDs
        participant_ids = [x for x in self.features]

        # Creates a plot that shows the label for each window.
        fig, ax = plt.subplots(2, sharex=True)
        fig.suptitle('Generated states vs actual labels of participant {}'.format(participant_ids[part_index]))
        ax[0].add_collection(colored_lines_states)
        ax[0].yaxis.set_visible(False)
        ax[0].xaxis.set_visible(False)
        ax[1].add_collection(colored_lines_labels)
        ax[1].yaxis.set_visible(False)
        ax[0].autoscale_view()
        ax[1].autoscale_view()
        ax[0].title.set_text("HSMM hidden states")
        ax[1].title.set_text("Actual movement labels")
        plt.xlabel('Windows')
        plt.show()


# FUNCTIONS
def train_hsmm(features, Nmax, dim, nr_resamples):
    """
    Fit a Hidden Semi Markov Model on a list of sequences.
    Parameters
    ------
    features : dictionary
        dictionary filled with feature dataframes
    Nmax : int
        Maximum number of states
    dim : int
        dimensions of the input feature dataframes
    nr_resamples : int
        Number of times the model is resampled on all data
    Returns
    ------
    model : pyhsmm model
        The resampled model
    """
    model = initialize_model(Nmax, dim)

    for participant in features.values():
        model.add_data(participant)

    for idx in progprint_xrange(nr_resamples):
        model.resample_model()

    return model


def initialize_model(Nmax, dim):
    """
    Initialize a HSMM model.
    Parameters
    ------
    Nmax : int
        Maximum number of states
    dim : int
        dimensions of the input feature dataframes
    Returns
    ------
    model : pyhsmm model
        The initial model
    """
    obs_hypparams = {'mu_0': np.zeros(dim),  # mean of gaussians
                     'sigma_0': np.eye(dim),  # std of gaussians
                     'kappa_0': 0.25,
                     'nu_0': dim + 2}

    # duration is going to be poisson, so prior is a gamma distribution
    # (params alpha beta)
    expected_lam = 30
    dur_hypparams = {'alpha_0': 2 * expected_lam,
                     'beta_0': 2}
    obs_distns = [distributions.Gaussian(**obs_hypparams) for state in range(Nmax)]
    dur_distns = [distributions.PoissonDuration(**dur_hypparams) for state in range(Nmax)]

    model = pyhsmm.models.WeakLimitHDPHSMM(
        alpha=6., gamma=6.,  # priors
        init_state_concentration=6.,  # alpha0 for the initial state
        obs_distns=obs_distns,
        dur_distns=dur_distns)
    return model


def get_label_occurrence_per_state(state_sequences, label_sequences):
    """
    Returns the occurrence for each label per hidden state.
    Parameters
    ------
    state_sequences : list
        list containing all of the hidden state sequences from every participant
    label_sequences : list
        list containing all of the label sequences from every participant
    Returns
    ------
    occurrence_count : dict
        A dictionary with the hidden states as keys and the labels for each movement window assinged to those states as values
    """
    
    occurrence_count = {}
    for part_index in range(len(label_sequences)):
        for window_index in range(len(label_sequences[part_index])):
            if state_sequences[part_index][window_index] in occurrence_count:
                occurrence_count[state_sequences[part_index][window_index]].append(
                    label_sequences[part_index][window_index])
            else:
                occurrence_count[state_sequences[part_index][window_index]] = [
                    label_sequences[part_index][window_index]]
    return occurrence_count


def get_state_to_label_dict(label_occurrence_per_state):
    """
    Returns a dictionary which asigns the most occuring label for a hidden state as it's label
    Parameters
    ------
    label_occurrence_per_state : dict
        A dictionary with the hidden states as keys and the labels for each movement window assinged to those states as values
    Returns
    ------
    state_to_label_dict : dict
        A dictionary with the hidden states as keys and the most occuring label for that hidden state as the value
    """
    
    state_to_label_dict = {}
    for state in label_occurrence_per_state:
        # Takes the most occurring (mode) label for each state
        state_to_label_dict[state] = stats.mode(label_occurrence_per_state[state])[0][0]
    return state_to_label_dict


def transform_state_sequence(state_to_label_dict, state_sequence):
    """
    Conferts the integer hidden states ID's to string labels for a single participant, 
    based on which label occurs the most in each state, using a dictionary
    Parameters
    ------
    state_to_label_dict : dict
        A dictionary with the hidden states as keys and the most occuring label for that hidden state as the value
    state_sequence : list
        list containing a  hidden state sequence from a participant
    Returns
    ------
    A state sequence with the most occuring label in those hidden states as the labels for those hidden states
    """
    
    return [state_to_label_dict[window] for window in state_sequence]


def assign_label_to_states(state_sequences, labels_as_strings):
    """
    Conferts the integer hidden states ID's to string labels, based on which label occurs the most in each state,
    using a dictionary
    Parameters
    ------
    state_sequences : list
        list containing all of the hidden state sequences from every participant
    labels_as_strings : list
        A list containing all of the label sequences in string form.
    Returns
    ------
    A list containing all of the state sequences of the dataset transformed into label based ID's.
    """
    
    label_occurrence_per_state = get_label_occurrence_per_state(state_sequences, labels_as_strings)
    state_to_label_dict = get_state_to_label_dict(label_occurrence_per_state)
    return [transform_state_sequence(state_to_label_dict, sequence) for sequence in state_sequences]
