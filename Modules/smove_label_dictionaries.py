# This module contains dictionaries used on the results from a HSMM model made with SMOVE data

# DICTIONARIES
label_color_dict = {0.0 : "#E000FF",   # (Purple) Lab taks 
                    1.0 : "#BD00D7",   #   - 8m
                    2.0 : "#9E00B4",   #   - L-task
                    3.0 : "#830095",   #   - Irregular_walking
                    4.0 : "#630070",   #   - 8-task
                    100.0 : "#F4D4FF", # (pink) Lying
                    199.0 : "#F4D4FF", #   - Other_lying (ex. chekcing phone)
                    200.0 : "#0055FF", # (Blue) Sitting 
                    201.0 : "#0055FF", #    - Sitting_lying (couch)
                    219.0 : "#0055FF", #    - Sitting_lying_others
                    220.0 : "#0055FF", #    - Sitting_straight
                    221.0 : "#0055FF", #    - Eating 
                    222.0 : "#0055FF", #    - Sitting_cooking
                    223.0 : "#0055FF", #    - Playing_cards
                    224.0 : "#0055FF", #    - Computer_working
                    225.0 : "#0055FF", #    - Putting_shoes
                    226.0 : "#0055FF", #    - Checking_phone
                    299.0 : "#0055FF", #    - Sitting_others
                    300.0 : "#00F3FF", # (Cyan) Cycling
                    400.0 : "#13FF00", # (Green) Standing
                    401.0 : "#13FF00", #    - Standing_cooking
                    402.0 : "#13FF00", #    - Standing_holding_something
                    403.0 : "#13FF00", #    - Standing_getting_dress
                    499.0 : "#13FF00", #    - Standing_others
                    500.0 : "#FFF300", # (Yellow) Moving_around
                    501.0 : "#FFF300", #    - Cooking
                    502.0 : "#FFF300", #    - Doing_dishes
                    503.0 : "#FFF300", #    - Cleaning_floor
                    504.0 : "#FFF300", #    - Moving_carrying_something
                    505.0 : "#FFF300", #    - Opening_closing_door
                    599.0 : "#FFF300", #    - Moving_around_others
                    600.0 : "#FFAA00", # (Orange) Walking
                    601.0 : "#FFAA00", #    - Upstairs 
                    602.0 : "#FFAA00", #    - Downstairs
                    603.0 : "#FFAA00", #    - Walking_holding_something
                    604.0 : "#FFAA00", # ?
                    699.0 : "#FFAA00", #    - Walking_others
                    700.0 : "#FF0000", # (Red) Other activities
                    701.0 : "#FF0000", #    - Putting_pants_sitting_to_standing
                    702.0 : "#FF0000", #    - Putting_shoes_kneeling
                    800.0 : "#000000"} # (Black) Other events

label_dict = {0.0: 'Lab_tasks',
              1.0: '8m',
              2.0: 'L_task',
              3.0: 'Irregular_walking',
              4.0: '8_task',
              100.0: 'Lying',
              199.0: 'Other_lying',
              200.0: 'Sitting',
              201.0: 'Sitting_lying',
              219.0: 'Sitting_lying_others',
              220.0: 'Sitting_straight',
              221.0: 'Eating',
              222.0: 'Sitting_cooking',
              223.0: 'Playing_cards',
              224.0: 'Computer_working',
              225.0: 'Putting_shoes',
              226.0: 'Checking_phone',
              299.0: 'Sitting_others',
              300.0: 'Cycling',
              400.0: 'Standing',
              401.0: 'Standing_cooking',
              402.0: 'Standing_holding_something',
              403.0: 'Standing_getting_dress',
              499.0: 'Standing_others',
              500.0: 'Moving_around',
              501.0: 'Cooking',
              502.0: 'Doing_dishes',
              503.0: 'Cleaning_floor',
              504.0: 'Moving_carrying_something',
              505.0: 'Opening_closing_door',
              599.0: 'Moving_around_others',
              600.0: 'Walking', 
              601.0: 'Upstairs',
              602.0: 'Downstairs',
              603.0: 'Walking_holding_something',
              604.0: "Unkown_walking", # ?
              699.0: 'Walking_others',
              700.0: 'Other_activities',
              701.0: 'Putting_pants_sitting_to_standing',
              702.0: 'Putting_shoes_kneeling',
              800.0: 'Other_events'}

label_simplified_dict = {0.0 : "Lab_tasks",          # Lab tasks 
                         1.0 : "Lab_tasks",          #    - 8m
                         2.0 : "Lab_tasks",          #    - L-task
                         3.0 : "Lab_tasks",          #    - Irregular_walking
                         4.0 : "Lab_tasks",          #    - 8-task
                         100.0 : "Lying",            # Lying
                         199.0 : "Lying",            #    - Other_lying (ex. chekcing phone)
                         200.0 : "Sitting",          # Sitting 
                         201.0 : "Sitting",          #    - Sitting_lying (couch)
                         219.0 : "Sitting",          #    - Sitting_lying_others
                         220.0 : "Sitting",          #    - Sitting_straight
                         221.0 : "Sitting",          #    - Eating 
                         222.0 : "Sitting",          #    - Sitting_cooking
                         223.0 : "Sitting",          #    - Playing_cards
                         224.0 : "Sitting",          #    - Computer_working
                         225.0 : "Sitting",          #    - Putting_shoes
                         226.0 : "Sitting",          #    - Checking_phone
                         299.0 : "Sitting",          #    - Sitting_others
                         300.0 : "Cycling",          # Cycling
                         400.0 : "Standing",         # Standing
                         401.0 : "Standing",         #    - Standing_cooking
                         402.0 : "Standing",         #    - Standing_holding_something
                         403.0 : "Standing",         #    - Standing_getting_dress
                         499.0 : "Standing",         #    - Standing_others
                         500.0 : "Moving_around",    # Moving_around
                         501.0 : "Moving_around",    #    - Cooking
                         502.0 : "Moving_around",    #    - Doing_dishes
                         503.0 : "Moving_around",    #    - Cleaning_floor
                         504.0 : "Moving_around",    #    - Moving_carrying_something
                         505.0 : "Moving_around",    #    - Opening_closing_door
                         599.0 : "Moving_around",    #    - Moving_around_others
                         600.0 : "Walking",          # Walking
                         601.0 : "Walking",          #    - Upstairs 
                         602.0 : "Walking",          #    - Downstairs
                         603.0 : "Walking",          #    - Walking_holding_something
                         604.0 : "Walking",          #    - ?
                         699.0 : "Walking",          #    - Walking_others
                         700.0 : "Other_activities", # Other activities
                         701.0 : "Other_activities", #    - Putting_pants_sitting_to_standing
                         702.0 : "Other_activities", #    - Putting_shoes_kneeling
                         800.0 : "Other_events"}     # Other events

state_color_dict = {"Walking" : "#FFAA00", # ORANGE
                    "Lying" : "#F4D4FF", # PINK
                    "Cycling" : "#00F3FF", # CYAN 
                    "Sitting" : "#0055FF", # BLUE 
                    "Standing" : "#13FF00", # GREEN 
                    "Moving_around" : "#FFF300", # YELLOW
                    "Other_activities" : "#FF0000", # RED
                    "Lab_tasks" : "#E000FF", # PURPLE
                    "Other_events" : "#000000"} # BLACK 
